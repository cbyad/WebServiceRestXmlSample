package com.estiam.customerapp.services;

import com.estiam.customerapp.model.Customer;
import com.estiam.customerapp.model.Invoice;

import java.util.List;

/**
 * Created by cb_mac on 16/11/17.
 */
public abstract class InvoiceListCallback implements Callback{

    public abstract void onSuccess(final Invoice invoices);
}
