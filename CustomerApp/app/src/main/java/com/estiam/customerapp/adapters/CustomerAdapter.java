package com.estiam.customerapp.adapters;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.estiam.customerapp.R;
import com.estiam.customerapp.activities.CustomerListActivity;
import com.estiam.customerapp.model.Customer;

import java.util.List;

/**
 * Created by cb_mac on 07/09/17.
 */
public class CustomerAdapter extends RecyclerView.Adapter<CustomerAdapter.MyViewHolder> {

private List<Customer> customers ;

private CustomerListActivity activity ;

  public CustomerAdapter(List<Customer> customers, CustomerListActivity context){
      this.customers=customers;
      activity=context;
  }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cell_customer_block,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Customer customer = customers.get(position);
        holder.display(customer);
    }

    @Override
    public int getItemCount() {
        return customers.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView firstName;
        private TextView lastName ;

        private Customer currentCustomer ;

        public MyViewHolder(final View itemView) {
            super(itemView);
            firstName=((TextView) itemView.findViewById(R.id.customer_fstName));
            lastName=((TextView) itemView.findViewById(R.id.customer_lstName));

            /*
            * Here action to perfom when an item is clicked
            * */
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String firstname = firstName.getText().toString();
                    String lastname = lastName.getText().toString();
                    activity.displayCustomerSelectedDetails(firstname,lastname);
                }
            });

            itemView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if(event.getAction() == MotionEvent.ACTION_DOWN)
                    {
                        v.setBackgroundColor(Color.rgb(63,81,181));
                    }
                    else  if (event.getAction() == MotionEvent.ACTION_UP ||
                            event.getAction() == MotionEvent.ACTION_CANCEL) {

                        v.setBackgroundColor(Color.TRANSPARENT);
                    }
                     return false ;
                }
            });

        }


        public void display(Customer customer){
            currentCustomer=customer;
            firstName.setText(customer.getFirstname());
            lastName.setText(customer.getLastname());
        }
    }
    }

