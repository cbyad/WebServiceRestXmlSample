package com.estiam.customerapp.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.estiam.customerapp.R;
import com.estiam.customerapp.activities.CustomerDetailsActivity;
import com.estiam.customerapp.model.Invoice;

/**
 * Created by cb_mac on 21/09/17.
 */
public class InvoiceAdapter extends RecyclerView.Adapter<InvoiceAdapter.MyViewHolder> {

private Invoice invoices ;
    int i =0 ;

private CustomerDetailsActivity activity ;


  public InvoiceAdapter(Invoice invoices, CustomerDetailsActivity context){
      this.invoices=invoices;
      activity=context;
  }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.cell_invoice_block,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Invoice invoice = invoices;
        holder.display(invoice,position);
    }

    @Override
    public int getItemCount() {return invoices.productList.size();}

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView productId;
        private TextView productName ;
        private TextView cost ;
        private TextView qte ;

        private Invoice currentInvoice ;

        public MyViewHolder(final View itemView) {
            super(itemView);

            productId=((TextView) itemView.findViewById(R.id.prodid_block));
            productName=((TextView) itemView.findViewById(R.id.prodname_block));
            cost=((TextView) itemView.findViewById(R.id.cost_block));
            qte=((TextView) itemView.findViewById(R.id.qte_block));
        }

        public void display(Invoice invoice,int i){
            currentInvoice=invoice;
            productId.setText(String.valueOf(invoice.productList.get(i)));
            productName.setText("                    "+invoice.nameProd.get(i));

            if(productName.getText().toString().length()<30 && productName.getText().toString().length()>8){
                cost.setText("                                 "+String.valueOf(invoice.costList.get(i)));
                qte.setText(String.valueOf(invoice.quantityList.get(i)));

            }
            else {
                cost.setText("                        " + String.valueOf(invoice.costList.get(i)));
                qte.setText(String.valueOf(invoice.quantityList.get(i)));
            }
        }
    }
    }

