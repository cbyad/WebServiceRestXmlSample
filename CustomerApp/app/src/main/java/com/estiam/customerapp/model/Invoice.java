package com.estiam.customerapp.model;

import android.icu.text.DecimalFormat;

import java.util.ArrayList;

/**
 * Created by cb_mac on 21/09/17.
 */
public class Invoice {

    public  ArrayList<Integer> productList ;
    public  ArrayList<Integer> quantityList;
    public  ArrayList<Double> costList ;
    public ArrayList<String> nameProd;

    private double total;


    public double getTotal(){
        if(total==0.0) {
            for (int i = 0; i < productList.size(); i++) {
                total += quantityList.get(i) * costList.get(i);
            }
            return total;
        }
        return total;
    }

    public Invoice() {
        this.productList=new ArrayList<>();
        this.quantityList=new ArrayList<>();
        this.costList=new ArrayList<>();
        this.nameProd=new ArrayList<>();
        this.total=0.0;
    }

}
