package com.estiam.customerapp.services;

/**
 * Created by cb_mac on 07/09/17.
 */
public interface Callback {
        void onError(int statusCode, String error);
}
