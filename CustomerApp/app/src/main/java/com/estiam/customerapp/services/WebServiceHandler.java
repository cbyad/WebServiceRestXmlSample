package com.estiam.customerapp.services;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.estiam.customerapp.model.Customer;
import com.estiam.customerapp.model.Invoice;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by cb_mac on 07/09/17.
 */

/*
*This Class is used to deal with all calls using web service
* Only on object is required by call ---> [Singleton pattern]
*/
public class WebServiceHandler {

    public final static int INVOICENOTFOUND = -1 ; // customer without invoice

    private static WebServiceHandler instance ;
    private Context context ;
    ArrayList<Customer> customers ;
    Customer currentCustomer= new Customer();

    private WebServiceHandler(Context context){
        this.context = context ;
    }

    public static synchronized WebServiceHandler getInstance(Context context){
        if(instance==null){
            instance= new WebServiceHandler(context);
        }
        return instance ;
    }


    public void customersList (final CustomersListCallback callback){

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {

                    URL url = new URL("http://www.thomas-bayer.com/sqlrest/CUSTOMER");

                    HttpURLConnection con = (HttpURLConnection) url.openConnection();
                    con.setRequestMethod("GET");

                    final int responseCode = con.getResponseCode();
                    if (responseCode == HttpURLConnection.HTTP_OK) {
                        String response = "";
                        BufferedReader is = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String line = null;
                        while ((line = is.readLine()) != null) {
                            response += line;
                        }
                        is.close();

                        // Here we Parse the xml response using Dom
                        try {
                            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                            DocumentBuilder db = dbf.newDocumentBuilder();
                            InputSource inputSource = new InputSource();
                            inputSource.setCharacterStream(new StringReader(response));

                            Document doc = db.parse(inputSource);
                            NodeList nodes = doc.getElementsByTagName("CUSTOMER");

                            customers = new ArrayList<>();
                            for (int i = 0; i < nodes.getLength(); i++) {

                                Element element = (Element) nodes.item(i);
                                int id = Integer.parseInt(element.getTextContent());

                                // Launch now a new request to join specific customer
                                Customer current = getCustomerDetails(id);
                                customers.add(current);
                            }
                        }

                        catch (Exception e){
                            e.getMessage();
                        }

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                               callback.onSuccess(customers);
                            }
                        });
                    } else if (responseCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                        callback.onError(responseCode,"Not permitted");
                    }
                    else {
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onError(responseCode, null);
                            }
                        });
                    }
                } catch (final IOException e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onError(-1, e.getMessage());
                        }
                    });
                }
            }
        }).start();


    }

    /**
     *
     * @param id_
     * @return
     */
    public Customer getCustomerDetails(final int id_ ) {
        Customer customer = new Customer();

        try {

            URL url = new URL("http://www.thomas-bayer.com/sqlrest/CUSTOMER/" + id_ + "/");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            final int responseCode = con.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String response = "";
                BufferedReader is = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String line = null;

                while ((line = is.readLine()) != null) {
                    response += line;
                }
                is.close();

                // Here we Parse the xml response using Dom
                try {
                    DocumentBuilderFactory dbf =
                            DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    InputSource inputSource = new InputSource();
                    inputSource.setCharacterStream(new StringReader(response));

                    Document doc = db.parse(inputSource);
                    NodeList nodes = doc.getElementsByTagName("CUSTOMER");

                    Element element = (Element) nodes.item(0);
                    NodeList id = element.getElementsByTagName("ID");
                    NodeList firstname = element.getElementsByTagName("FIRSTNAME");
                    NodeList lastname = element.getElementsByTagName("LASTNAME");
                    NodeList street = element.getElementsByTagName("STREET");
                    NodeList city = element.getElementsByTagName("CITY");

                    customer.setId(Integer.parseInt(id.item(0).getTextContent()));
                    customer.setFirstname(firstname.item(0).getTextContent());
                    customer.setLastname(lastname.item(0).getTextContent());
                    customer.setStreet(street.item(0).getTextContent());
                    customer.setCity(city.item(0).getTextContent());
                    customer.invoice=new Invoice();
                } catch (Exception e) {
                    e.getMessage();
                }
            }

        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return customer;
    }

    /**
     *
     * @param customerID
     * @return INVOICEID from a specific customerID
     */
    public int getInvoiceID(int customerID ) throws IOException {

        URL url = new URL("http://www.thomas-bayer.com/sqlrest/INVOICE/");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");

        final int invoiceCode = con.getResponseCode();
        System.out.println("yoo "+invoiceCode);
        if (invoiceCode == HttpURLConnection.HTTP_OK) {
            String reponse = "";
            BufferedReader is = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String line = null;
            while ((line = is.readLine()) != null) {
                reponse += line;
            }
            is.close();
            try {
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                InputSource inputSource = new InputSource();
                inputSource.setCharacterStream(new StringReader(reponse));

                Document doc = db.parse(inputSource);
                NodeList nodes = doc.getElementsByTagName("INVOICE");

                for (int i = 0; i < nodes.getLength(); i++) {

                    Element element = (Element) nodes.item(i);
                    int id = Integer.parseInt(element.getTextContent());
                    String currInvoice = "http://www.thomas-bayer.com/sqlrest/INVOICE/" + id + "/";

                    HttpURLConnection con1 = (HttpURLConnection) new URL(currInvoice).openConnection();
                    con1.setRequestMethod("GET");
                    if (con1.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        String reponse1 = "";
                        BufferedReader is1 = new BufferedReader(new InputStreamReader(con1.getInputStream()));
                        String line1 = null;
                        while ((line1 = is1.readLine()) != null) {
                            reponse1 += line1;
                        }
                        is1.close();

                        try {
                            DocumentBuilderFactory dbf1 =
                                    DocumentBuilderFactory.newInstance();
                            DocumentBuilder db1 = dbf1.newDocumentBuilder();
                            InputSource inputSource1 = new InputSource();
                            inputSource1.setCharacterStream(new StringReader(reponse1));

                            Document doc1 = db1.parse(inputSource1);
                            NodeList nodes1 = doc1.getElementsByTagName("INVOICE");

                            Element element1 = (Element) nodes1.item(0);
                            NodeList idCustomer = element1.getElementsByTagName("CUSTOMERID");
                            NodeList idInvoce = element1.getElementsByTagName("ID");

                            int idcus = Integer.parseInt(idCustomer.item(0).getTextContent());
                            int idIv = Integer.parseInt(idInvoce.item(0).getTextContent());

                            if(customerID==idcus){
                                return idIv;
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }

                    }

                }
            } catch (Exception e) {
                e.getMessage();
            }
        }
    return INVOICENOTFOUND ;
    }


    public String getNameProd( int productID ) {
        String nameP="";
        try {

            URL url = new URL("http://www.thomas-bayer.com/sqlrest/PRODUCT/" + productID+ "/");
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");

            final int responseCode = con.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                String response = "";
                BufferedReader is = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String line = null;

                while ((line = is.readLine()) != null) {
                    response += line;
                }
                is.close();

                // Here we Parse the xml response using Dom
                try {
                    DocumentBuilderFactory dbf =
                            DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    InputSource inputSource = new InputSource();
                    inputSource.setCharacterStream(new StringReader(response));

                    Document doc = db.parse(inputSource);
                    NodeList nodes = doc.getElementsByTagName("PRODUCT");

                    Element element = (Element) nodes.item(0);
                    NodeList nameprod = element.getElementsByTagName("NAME");
                    nameP= nameprod.item(0).getTextContent();
                    return nameP;

                } catch (Exception e) {
                    e.getMessage();
                }
            }

        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nameP;
    }



    public  Customer findCustomerByID(int id){
        for (Customer c : customers){
            if(c.getId()==id){
                return c;
            }
        }
        return null ;
    }

    public void invoicesList(final int customerId ,final InvoiceListCallback callback)   {
          currentCustomer =findCustomerByID(customerId);

        new Thread(new Runnable() {
            @Override
            public void run() {

                int invoiceID = -1;
                try {
                    invoiceID = getInvoiceID(customerId);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if(invoiceID==INVOICENOTFOUND){
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onError(-2,"invoice not found");
                        }
                    });
                }

                try{
                URL url = new URL("http://www.thomas-bayer.com/sqlrest/ITEM/" + invoiceID);
                HttpURLConnection con = (HttpURLConnection) url.openConnection();
                con.setRequestMethod("GET");

                try {
                    if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                        String reponse = "";
                        BufferedReader is = new BufferedReader(new InputStreamReader(con.getInputStream()));
                        String line = null;
                        while ((line = is.readLine()) != null) {
                            reponse += line;
                        }
                        is.close();

                        try {
                            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                            DocumentBuilder db = dbf.newDocumentBuilder();
                            InputSource inputSource = new InputSource();
                            inputSource.setCharacterStream(new StringReader(reponse));

                            Document doc = db.parse(inputSource);
                            NodeList nodes = doc.getElementsByTagName("ITEM");

                            Element element = (Element) nodes.item(0); // just one ITEM in the document


                            NodeList products = element.getElementsByTagName("PRODUCTID");
                            NodeList quantities = element.getElementsByTagName("QUANTITY");
                            NodeList costs = element.getElementsByTagName("COST");


                            for (int i = 0; i < products.getLength() /* i<quantities.getLength() ...*/; i++) {
                                Element currProd = (Element) products.item(i);
                                Element currQuant = (Element) quantities.item(i);
                                Element currCost = (Element) costs.item(i);

                                int productID = Integer.parseInt(currProd.getTextContent());
                                int quantity = Integer.parseInt(currQuant.getTextContent());
                                double cost = Double.parseDouble(currCost.getTextContent());

                                currentCustomer.invoice.productList.add(productID);
                                currentCustomer.invoice.quantityList.add(quantity);
                                currentCustomer.invoice.costList.add(cost);
                                currentCustomer.invoice.nameProd.add(getNameProd(productID));
                            }

                        } catch (Exception e) {
                            e.getMessage();
                        }

                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {
                                callback.onSuccess(currentCustomer.invoice);
                            }
                        });
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }

            }catch (final IOException e) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            callback.onError(-1, e.getMessage());
                        }
                    });
                }
            }

        }).start();
    }
}
