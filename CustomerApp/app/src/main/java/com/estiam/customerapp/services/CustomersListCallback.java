package com.estiam.customerapp.services;

import com.estiam.customerapp.model.Customer;

import java.util.List;

/**
 * Created by cb_mac on 07/09/17.
 */
public abstract class CustomersListCallback implements Callback{

    public abstract void onSuccess(List<Customer> customers);

}
