package com.estiam.customerapp.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.estiam.customerapp.R;
import com.estiam.customerapp.adapters.CustomerAdapter;
import com.estiam.customerapp.adapters.SimpleDividerItemDecoration;
import com.estiam.customerapp.model.Customer;
import com.estiam.customerapp.services.CustomersListCallback;
import com.estiam.customerapp.services.WebServiceHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CustomerListActivity extends AppCompatActivity {

    RecyclerView customerList ;
    ProgressBar progressBar ;
    static List<Customer> ct ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_main_activity);
        setTitle("Customers");
        init();
    }

    // Init All Components in Our Layout
    private void init() {
        customerList = (RecyclerView) findViewById(R.id.list_view);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);
        Toast.makeText(CustomerListActivity.this, "Please wait...",Toast.LENGTH_SHORT).show();
        initCustomerList();
    }


    private void initCustomerList() {

        WebServiceHandler service = WebServiceHandler.getInstance(this);
        service.customersList(new CustomersListCallback() {
            @Override
            public void onSuccess(final List<Customer> customers) {
                progressBar.setVisibility(View.GONE);
                customerList.setLayoutManager(new LinearLayoutManager(CustomerListActivity.this));
                customerList.setAdapter(new CustomerAdapter(customers,CustomerListActivity.this));
                customerList.addItemDecoration(new SimpleDividerItemDecoration(CustomerListActivity.this));
                ct=new ArrayList<>(customers);
            }

            @Override
            public void onError(int statusCode, String error) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(CustomerListActivity.this, "An internet error occured", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void displayCustomerSelectedDetails(String first , String  last) {
        Customer tosend =findCustomerByName(first,last);
        Intent intent = new Intent(CustomerListActivity.this,CustomerDetailsActivity.class);
        intent.putExtra("id",tosend.getId());
        intent.putExtra("firstname",tosend.getFirstname());
        intent.putExtra("lastname",tosend.getLastname());
        intent.putExtra("street",tosend.getStreet());
        intent.putExtra("city",tosend.getCity());
        startActivity(intent);
    }

    public static Customer findCustomerByName(String firstname ,String lastname){
        for (Customer c : ct){
            if(c.getLastname().equals(lastname) && c.getFirstname().equals(firstname)){
                return c;
            }
        }
        return null ;
    }

}
