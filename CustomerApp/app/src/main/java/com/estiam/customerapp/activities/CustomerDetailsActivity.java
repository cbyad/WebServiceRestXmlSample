package com.estiam.customerapp.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.estiam.customerapp.R;

import com.estiam.customerapp.adapters.InvoiceAdapter;
import com.estiam.customerapp.adapters.SimpleDividerItemDecoration;
import com.estiam.customerapp.model.Invoice;
import com.estiam.customerapp.services.InvoiceListCallback;
import com.estiam.customerapp.services.WebServiceHandler;


/**
 * Created by cb_mac on 07/09/17.
 */
public class CustomerDetailsActivity extends Activity {

    RecyclerView invoiceList ;
    ProgressBar progressBar ;
    TextView id  ;
    TextView firstname ;
    TextView lastname ;
    TextView street ;
    TextView city ;
    TextView total ;
    int idCustomer ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.customer_selected);


        int idReceived = this.getIntent().getIntExtra("id",0);
        final String firstNameReceived = this.getIntent().getStringExtra("firstname");
        final String lastNameReceived = this.getIntent().getStringExtra("lastname");
        String streetReceived = this.getIntent().getStringExtra("street");
        String cityReceived = this.getIntent().getStringExtra("city");
        idCustomer=idReceived;

        init();
        firstname.setText(firstNameReceived);
        lastname.setText(lastNameReceived);
        street.setText(streetReceived);
        city.setText(cityReceived);
        id.setText(String.valueOf(idReceived));

        WebServiceHandler service = WebServiceHandler.getInstance(this);

        service.invoicesList(idCustomer, new InvoiceListCallback() {
            @Override
            public void onSuccess(final Invoice invoices) {
                System.out.println("Success ");

                progressBar.setVisibility(View.GONE);
                invoiceList.setLayoutManager(new LinearLayoutManager(CustomerDetailsActivity.this));
                invoiceList.setAdapter(new InvoiceAdapter(invoices, CustomerDetailsActivity.this));
                invoiceList.addItemDecoration(new SimpleDividerItemDecoration(CustomerDetailsActivity.this));
                total.setText(String.valueOf(invoices.getTotal()));
            }

            @Override
            public void onError(int statusCode, String error) {
                if (statusCode == -2) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(CustomerDetailsActivity.this, firstNameReceived+" "+lastNameReceived +" has no bill", Toast.LENGTH_LONG).show();
                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(CustomerDetailsActivity.this, "Internet failed", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void init()  {
        id=(TextView)findViewById(R.id.detail_id);
        firstname=(TextView)findViewById(R.id.detail_firstname);
        lastname=(TextView)findViewById(R.id.detail_lastname);
        street=(TextView)findViewById(R.id.detail_street);
        city=(TextView)findViewById(R.id.detail_city);
        total=(TextView)findViewById(R.id.detail_total);
        invoiceList = (RecyclerView) findViewById(R.id.listview_invoice);
        progressBar = (ProgressBar)findViewById(R.id.progressBarInvoice);
        progressBar.setVisibility(View.VISIBLE);
    }

}
